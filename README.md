# speedtester
Docker image running a [LibreSpeed SpeedTest](https://github.com/librespeed/speedtest) example instance suitable for internal speed testing. Originally based on Ubuntu images, it was refactored to Alpine which still supports arm32v6 for the Raspberry Pi. It is much smaller and faster as a result. Older Raspberry Pi's have a network speed limitation that this speed test can measure.

Since the creation of this Docker image, there is now a fully featured official LibreSpeed docker image: https://github.com/librespeed/speedtest/blob/master/doc_docker.md

# Building Your Own
1. git clone https://gitlab.com/doritoes/speedtester
2. cd speedtester
3. docker build -t speedtester:latest .
4. docker run -d --name speedtester -p80:8080 --restart=always speedtester
# Prebuilt Image
docker run -d --name speedtester -p80:8080 --restart=always doritoes/speedtester

# Raspberry Pi (and other compatible ARM systems)
**See https://en.wikipedia.org/wiki/Raspberry_Pi#Specifications for details about which modules use which ARM instruction set**
## 32-bit OS arm32v6
For the Pi 1, original Pi 2 B, Compute Module 1, Pi Zero and Pi Zero W

The armhf organization is deprecated in favor of the more-specific arm32v6 and arm32v7 organizations. ([Link](https://github.com/docker-library/official-images#architectures-other-than-amd64))
### Build Your Own
1. git clone https://gitlab.com/doritoes/speedtester
2. cd speedtester
3. docker build -t speedtester:latest -f Dockerfile.arm32v6 .
4. docker run -d --name speedtester -p80:8080 --restart=always speedtester
### Prebuilt Image
docker run -d --name speedtester -p80:8080 --restart=always doritoes/speedtester:arm32v6

## 32-bit OS armhf arm32v7/arm32v8
### Build Your Own
1. git clone https://gitlab.com/doritoes/speedtester
2. cd speedtester
3. docker build -t speedtester:latest -f Dockerfile.arm32v7 .
4. docker run -d --name speedtester -p80:8080 --restart=always speedtester
### Prebuilt Image
docker run -d --name speedtester -p80:8080 --restart=always doritoes/speedtester:arm32v7

## 64-bit OS arm64v8
Raspbery Pi 2B 1.2 and later models support 64-bit OS
### Build Your Own
1. git clone https://gitlab.com/doritoes/speedtester
2. cd speedtester
3. docker build -t speedtester:latest -f Dockerfile.arm64v8 .
4. docker run -d --name speedtester -p80:8080 --restart=always speedtester
### Prebuilt Image
docker run -d --name speedtester -p80:8080 --restart=always doritoes/speedtester:arm64v8
